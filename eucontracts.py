#
# One can directly query full-text search across EU contracts at:
#
#   https://ted.europa.eu/TED/search/expertSearch.do
#
# e.g., by entering the query
#
#   FT=[palantir]
#
# or
#
#   FT=["palantir technologies"]
#
# Alternatively, one can download tar.gz of XML files for each day's contracts
# (one file per contract) at:
#
#   ftp://ted.europa.eu/daily-packages/
#
# Alternatively, we can make use of the v2.0 API at
#
#   https://ted.europa.eu/api/v2.0/notices/search?q=FT%3D%5Bpalantir%5D
#
# by encoding the query string '=' signs as %3D, the '[' as %5B, and the ']' as
# %5D.
#
import csv
import glob
import json
import requests
import xmltodict

# This list of language preference scores is somewhat arbitrary.
LANGUAGE_PREFERENCES = {'EN': 10, 'ES': 8, 'FR': 8, 'DE': 7, 'IT': 7}


MAP_FILES = {
    'AA_AUTHORITY_TYPE': 'data/aa_authority_type.json',
    'AC_AWARD_CRIT': 'data/ac_award_crit.json',
    'MA_MAIN_ACTIVITIES': 'data/ma_main_activities.json',
    'NC_CONTRACT_NATURE': 'data/nc_contract_nature.json',
    'NUTS': 'data/nuts.json',
    'PR_PROC': 'data/pr_proc.json',
    'RP_REGULATION': 'data/rp_regulation.json',
    'TD_DOCUMENT_TYPE': 'data/td_document_type.json',
    'TY_TYPE_BID': 'data/ty_type_bid.json'
}


def replace_type_with_code(item, orig_key, new_key, code_dict):
    if orig_key not in item:
        return
    value = item[orig_key]
    code = value['@CODE']
    if '#text' in value:
        text = value['#text']
        if code in code_dict:
            if code_dict[code] != text:
                print('Code {} text mismatch: {} != {}'.format(
                    code, text, code_dict[code]))
        else:
            code_dict[code] = text
    item[new_key] = code
    del item[orig_key]


def replace_type_list_with_codes(item, orig_key, new_key, code_dict):
    if orig_key not in item:
        return
    value = item[orig_key]
    code_list = []
    value_list = value if isinstance(value, list) else [value]
    for value in value_list:
        code = value['@CODE']
        if '#text' in value:
            text = value['#text']
            if code in code_dict:
                if code_dict[code] != text:
                    print('Code {} text mismatch: {} != {}'.format(
                        code, text, code_dict[code]))
            else:
                code_dict[code] = text
        code_list.append(code)

    item[new_key] = '; '.join(code_list)
    del item[orig_key]


def join_paragraph(para):
    def extract_text(line):
        return line['#text'] if isinstance(line, dict) else line

    para = para if isinstance(para, list) else [para]
    para = [extract_text(line) for line in para if line]
    if len(para):
        return '; '.join(para)
    else:
        return None


def simplify_coded_data_section(coded_data, maps):
    if 'NOTICE_DATA' in coded_data:
        notice_data = coded_data['NOTICE_DATA']

        replace_type_list_with_codes(notice_data,
                                     'n2021:PERFORMANCE_NUTS',
                                     'performance_nuts', maps['NUTS'])
        replace_type_list_with_codes(notice_data, 'n2021:CA_CE_NUTS',
                                     'ca_ce_nuts', maps['NUTS'])
        replace_type_list_with_codes(notice_data,
                                     'n2021:TENDERER_NUTS',
                                     'tenderer_nuts', maps['NUTS'])

        if 'URI_LIST' in notice_data:
            uri_doc = notice_data['URI_LIST']['URI_DOC']
            if isinstance(uri_doc, dict):
                notice_data['uri'] = uri_doc['#text']
            elif isinstance(uri_doc, list):
                lang_state = {'score': -1, 'item': None}
                for item in uri_doc:
                    if item['@LG'] in LANGUAGE_PREFERENCES:
                        score = LANGUAGE_PREFERENCES[item['@LG']]
                        if score > lang_state['score']:
                            lang_state['score'] = score
                            lang_state['item'] = item['#text']
                    elif lang_state['score'] < 0:
                        lang_state['score'] = 0
                        lang_state['item'] = item['#text']
                if lang_state['item']:
                    notice_data['uri'] = lang_state['item']
            del notice_data['URI_LIST']

        if 'ISO_COUNTRY' in notice_data:
            notice_data['country'] = notice_data['ISO_COUNTRY']['@VALUE']
            del notice_data['ISO_COUNTRY']

        if 'ORIGINAL_CPV' in notice_data:
            original_cpv = notice_data['ORIGINAL_CPV']
            if isinstance(original_cpv, dict):
                notice_data['original_cpv'] = original_cpv['#text']
                del notice_data['ORIGINAL_CPV']
            elif isinstance(original_cpv, list):
                desc = '; '.join(item['#text'] for item in original_cpv)
                notice_data['original_cpv'] = desc
                del notice_data['ORIGINAL_CPV']

        coded_data['notice_data'] = notice_data
        del coded_data['NOTICE_DATA']

    if 'CODIF_DATA' in coded_data:
        codif_data = coded_data['CODIF_DATA']
        replace_type_with_code(codif_data, 'AA_AUTHORITY_TYPE',
                               'aa_authority', maps['AA_AUTHORITY_TYPE'])
        replace_type_with_code(codif_data, 'AC_AWARD_CRIT',
                               'ac_award_crit', maps['AC_AWARD_CRIT'])
        replace_type_with_code(codif_data, 'MA_MAIN_ACTIVITIES',
                               'ma_main_activities',
                               maps['MA_MAIN_ACTIVITIES'])
        replace_type_with_code(codif_data, 'NC_CONTRACT_NATURE',
                               'nc_contract_nature',
                               maps['NC_CONTRACT_NATURE'])
        replace_type_with_code(codif_data, 'PR_PROC', 'pr_proc',
                               maps['PR_PROC'])
        replace_type_with_code(codif_data, 'RP_REGULATION',
                               'rp_regulation', maps['RP_REGULATION'])
        replace_type_with_code(codif_data, 'TD_DOCUMENT_TYPE',
                               'td_document', maps['TD_DOCUMENT_TYPE'])
        replace_type_with_code(codif_data, 'TY_TYPE_BID',
                               'ty_type_bid', maps['TY_TYPE_BID'])
        coded_data['codif_data'] = codif_data
        del coded_data['CODIF_DATA']

    return coded_data


def simplify_form_section(form, maps):
    if 'NOTICE_UUID' in form:
        del form['NOTICE_UUID']
    if len(form.keys()) > 1:
        print('Error: FORM_SECTION had too many keys:')
        print(form.keys())
        return form

    form = form[list(form.keys())[0]]
    form = form if isinstance(form, list) else [form]
    for value in form:
        if 'AWARD_CONTRACT' in value:
            contract = value['AWARD_CONTRACT']
            if 'TITLE' in contract:
                contract['title'] = contract['TITLE']['P']
                del contract['TITLE']
            value['award_contract'] = contract
            del value['AWARD_CONTRACT']
        if 'CONTRACTING_BODY' in value:
            contracting_body = value['CONTRACTING_BODY']
            if 'ADDRESS_CONTRACTING_BODY' in contracting_body:
                address = contracting_body[
                    'ADDRESS_CONTRACTING_BODY']
                if 'COUNTRY' in address:
                    address['country'] = address['COUNTRY'][
                        '@VALUE']
                    del address['COUNTRY']
                replace_type_with_code(address, 'n2021:NUTS',
                                       'nuts', maps['NUTS'])
                contracting_body['address'] = address
                del contracting_body['ADDRESS_CONTRACTING_BODY']
            if 'CE_ACTIVITY' in contracting_body:
                contracting_body['ce_activity'] = contracting_body[
                    'CE_ACTIVITY']['@VALUE']
                del contracting_body['CE_ACTIVITY']
            value['contracting_body'] = contracting_body
            del value['CONTRACTING_BODY']
        if 'LEGAL_BASIS' in value:
            value['legal_basis'] = value['LEGAL_BASIS']['@VALUE']
            del value['LEGAL_BASIS']
        if 'MODIFICATIONS_CONTRACT' in value:
            contract = value['MODIFICATIONS_CONTRACT']
            if 'DESCRIPTION_PROCUREMENT' in contract:
                procurement = contract['DESCRIPTION_PROCUREMENT']
                replace_type_list_with_codes(
                    procurement, 'n2021:NUTS', 'nuts', maps['NUTS'])
                if 'MAIN_SITE' in procurement:
                    joined_para = join_paragraph(procurement['MAIN_SITE']['P'])
                    if joined_para:
                        procurement['main_site'] = joined_para
                    del procurement['MAIN_SITE']
                if 'SHORT_DESCR' in procurement:
                    joined_para = join_paragraph(
                        procurement['SHORT_DESCR']['P'])
                    if joined_para:
                        procurement['short_descr'] = joined_para
                    del procurement['SHORT_DESCR']
                contract['description_procurement'] = procurement
                del contract['DESCRIPTION_PROCUREMENT']
            if 'INFO_MODIFICATIONS' in contract:
                modifications = contract['INFO_MODIFICATIONS']
                if 'SHORT_DESCR' in modifications:
                    descr = modifications['SHORT_DESCR']
                    joined_para = join_paragraph(descr['P'])
                    if joined_para:
                        modifications['short_descr'] = joined_para
                    del modifications['SHORT_DESCR']
                contract['info_modifications'] = modifications
                del contract['INFO_MODIFICATIONS']
            value['modifications_contract'] = contract
            del value['MODIFICATIONS_CONTRACT']
        if 'OBJECT_CONTRACT' in value:
            contract = value['OBJECT_CONTRACT']
            if 'OBJECT_DESCR' in contract:
                descr = contract['OBJECT_DESCR']
                descr_values = descr if isinstance(descr, list) else [descr]
                for descr_value in descr_values:
                    if 'EU_PROGR_RELATED' in descr_value:
                        joined_para = join_paragraph(
                            descr_value['EU_PROGR_RELATED']['P'])
                        if joined_para:
                            descr_value['eu_progr_related'] = joined_para
                        del descr_value['EU_PROGR_RELATED']
                    if 'MAIN_SITE' in descr_value:
                        joined_para = join_paragraph(
                            descr_value['MAIN_SITE']['P'])
                        if joined_para:
                            descr_value['main_site'] = joined_para
                        del descr_value['MAIN_SITE']
                    if 'SHORT_DESCR' in descr_value:
                        joined_para = join_paragraph(
                            descr_value['SHORT_DESCR']['P'])
                        if joined_para:
                            descr_value['short_descr'] = joined_para
                        del descr_value['SHORT_DESCR']
                    if 'TITLE' in descr_value:
                        descr_value['title'] = descr_value['TITLE']['P']
                        del descr_value['TITLE']
                    replace_type_list_with_codes(
                        descr_value, 'n2021:NUTS', 'nuts', maps['NUTS'])
                contract['object_descr'] = descr_values
                del contract['OBJECT_DESCR']
            if 'REFERENCE_NUMBER' in contract:
                contract['reference_number'] = contract['REFERENCE_NUMBER']
                del contract['REFERENCE_NUMBER']
            if 'SHORT_DESCR' in contract:
                joined_para = join_paragraph(contract['SHORT_DESCR']['P'])
                if joined_para:
                    contract['short_descr'] = joined_para
                del contract['SHORT_DESCR']
            if 'TITLE' in contract:
                contract['title'] = contract['TITLE']['P']
                del contract['TITLE']
            if 'TYPE_CONTRACT' in contract:
                contract['type_contract'] = contract['TYPE_CONTRACT']['@CTYPE']
                del contract['TYPE_CONTRACT']
            value['object_contract'] = contract
            del value['OBJECT_CONTRACT']

    return form


def simplify_translation_section(translation, maps):
    if 'ML_TITLES' in translation:
        titles = translation['ML_TITLES']
        title = None
        if 'ML_TI_DOC' in titles:
            ti_doc = titles['ML_TI_DOC']
            if isinstance(ti_doc, list):
                lang_state = {'score': -1, 'item': None}
                for lang in ti_doc:
                    if lang['@LG'] in LANGUAGE_PREFERENCES:
                        score = LANGUAGE_PREFERENCES[lang['@LG']]
                        if score > lang_state['score']:
                            lang_state['score'] = score
                            lang_state['item'] = lang
                        elif score < 0:
                            lang_state['score'] = 0
                            lang_state['item'] = lang
                if lang_state['item']:
                    item = lang_state['item']
                    title = {
                        'language': item['@LG'],
                        'country': item['TI_CY'],
                        'town': item['TI_TOWN']
                    }
                    text_para = join_paragraph(item['TI_TEXT']['P'])
                    if text_para:
                        title['text'] = text_para
        if title:
            translation['title'] = title
        del translation['ML_TITLES']

    return translation


def convert_folder_to_json(folder_path):
    '''Converts unpacked .tar.gz of EU TED XMLs into a JSON array.'''
    # The top-level fields we immediately drop.
    FIELDS_TO_DROP = [
        '@xmlns:xlink', '@xmlns:xsi', '@xmlns', '@xmlns:n2021',
        '@xmlns:schemaLocation', '@xsi:schemaLocation', '@VERSION', '@EDITION',
        'LINKS_SECTION', 'TECHNICAL_SECTION'
    ]

    maps = {}
    for key in MAP_FILES:
        maps[key] = json.load(open(MAP_FILES[key]))

    data = []
    filenames = glob.glob('{}/**'.format(folder_path))
    for filename in filenames:
        with open(filename) as infile:
            doc = xmltodict.parse(infile.read())
            if 'TED_EXPORT' not in doc:
                continue
            doc = doc['TED_EXPORT']

            for field in FIELDS_TO_DROP:
                if field in doc:
                    del doc[field]

            if 'CODED_DATA_SECTION' in doc:
                coded_data = doc['CODED_DATA_SECTION']
                coded_data = simplify_coded_data_section(coded_data, maps)
                doc['coded_data'] = coded_data
                del doc['CODED_DATA_SECTION']

            if 'TRANSLATION_SECTION' in doc:
                translation = doc['TRANSLATION_SECTION']
                translation = simplify_translation_section(translation, maps)
                doc['translation'] = translation
                del doc['TRANSLATION_SECTION']

            if 'FORM_SECTION' in doc:
                form = doc['FORM_SECTION']
                form = simplify_form_section(form, maps)
                doc['form'] = form
                del doc['FORM_SECTION']

            data.append(doc)

    for key in MAP_FILES:
        with open(MAP_FILES[key], 'w') as outfile:
            json.dump(maps[key], outfile, indent=1)

    return data
